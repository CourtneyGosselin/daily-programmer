//https://leetcode.com/problems/two-sum/

/*
Given an array of integers, return indices of the two numbers such that they add
up to a specific target. You may assume that each input would have exactly one
solution, and you may not use the same element twice.
*/

/*
Example:

Given nums = [2, 7, 11, 15], target = 9,
Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].

*/ 

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
    for(var i = 0; nums.length > i; i++){
        for(var j = i+1; nums.length > j; j++){
            if(nums[i]+nums[j]==target){
                return [i, j];
            }
        }
    }
};

/* Testing Examples 
var nums =  [2,7,11, 15];
var target = 9;
*/
/*
var nums =  [3,2,4];
var target = 6;
*/
var nums = [0,4,3,0];
var target= 0;
console.log(twoSum(nums, target));
